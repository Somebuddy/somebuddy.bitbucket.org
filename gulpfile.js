var gulp = require("gulp");
var del = require("del");
var babel = require("gulp-babel");

var browserSync = require('browser-sync').create();
var serveConfig = require('./configs/bs-config.js');

var excluded_files = [
  '!./node_modules/*',
  '!./.c9/*',
  '!./.git/*',
];

var templateFiles = ['./index.html', './src/client/**/*.html'].concat(excluded_files);
var jsFiles = ['./src/**/*.js'].concat(excluded_files);
var sassFiles = ['./src/styles/**/*.sass', './src/styles/**/*.scss'].concat(excluded_files);
var cssFiles = ['./src/styles/**/*.css'].concat(excluded_files);

// Template preparing tasks
gulp.task('build:templates:clean', function() {
  return del(['./client/**/*.html']);
});

gulp.task('build:templates:copy', ['build:templates:clean'], function() {
  return gulp.src(templateFiles)
    .pipe(gulp.dest('./client'));
});

gulp.task('build:templates', ['build:templates:clean', 'build:templates:copy']);

// Preparing JavaScript files tasks
gulp.task('build:scripts:clean', function() {
  return del(['./client/**/*.js']);
});

gulp.task('build:js', ['build:scripts:clean'], function() {
  return gulp.src(jsFiles)
    .pipe(babel())
    .pipe(gulp.dest('./client'));
});

gulp.task('build:scripts', ['build:scripts:clean', 'build:js']);

// Preparing styles
gulp.task('build:styles:clean', function() {
  return del(['./client/**/*.css']);
});

gulp.task('build:sass', ['build:styles:clean'], function() {
  var sass = require('gulp-sass');

  return gulp.src(sassFiles)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("./client/styles"));
});

gulp.task('build:css', ['build:styles:clean'], function() {
  gulp.src(cssFiles)
    .pipe(gulp.dest('./client/styles'));
});

gulp.task('build:styles', ['build:styles:clean', 'build:sass', 'build:css']);

// Test tasks
gulp.task('test:scripts', ['build:scripts'], function() {
});

// Watch tasks
gulp.task('watch:scripts', ['build:scripts', 'test:scripts'], browserSync.reload);
gulp.task('watch:templates', ['build:templates'], browserSync.reload);
gulp.task('watch:styles', ['build:styles'], browserSync.reload);

// Main tasks
gulp.task("serve", ['build:templates', 'build:scripts', 'build:styles', 'test:scripts'], function() {
  browserSync.init(serveConfig);
  gulp.watch(jsFiles, ['watch:scripts']);
  gulp.watch(templateFiles, ['watch:templates']);
  gulp.watch(sassFiles, ['watch:styles']);
  gulp.watch(cssFiles, ['watch:styles']);
});

gulp.task("default", ['serve']);
