import { Dashboard } from '../apps/work-timer/dashboard';
import { Todos } from '../apps/work-timer/todos';


var TIMESETS = [
  {
    id: 1,
    comment: 'Added time set stub template',
    running: true,
    slots: [
      { id: 1 },
      { id: 2 }
    ],
  },
  {
    id: 2,
    comment: 'Configure service worker',
    running: false,
    slots: [
      { id: 3 },
      { id: 4 }
    ],
  },
  {
    id: 3,
    comment: 'Add time set widget base styles',
    running: false,
    slots: [
      { id: 5 },
      { id: 6 }
    ],
  },
  {
    id: 4,
    comment: 'Add time set example content',
    running: false,
    slots: [
      { id: 7 },
      { id: 8 }
    ],
  }
];

class WorkTimerPage extends React.Component {
  render() {
    return <section className="work-timer-app">
      <nav>
        <ul>
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">History</a></li>
          <li><a href="#">Statistics</a></li>
          <li><a href="#">Settings</a></li>
        </ul>
      </nav>
      <section className="content">
        <Dashboard sets={TIMESETS}/>
        <Todos />
      </section>

      <footer>
      </footer>
    </section>;
  }
}

export { WorkTimerPage };
