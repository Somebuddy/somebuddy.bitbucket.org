import { TimeSlotList } from './time-slot-widget';

class TimeSetWidget extends React.Component {
  render() {
    return <div className="widget time-set">
      <div className="useful-flag checked">
        <div className="icon"></div>
        <div className="label">is useful?</div>
      </div>

      <div className="comment variable">
        <div className="label">comment:</div>
        <div className="value">{ this.props.timeset.comment }</div>
      </div>

      <div className="start-time">
        <header>started at:</header>
        <div className="value">10:01</div>
      </div>

      <div className="timer summary">
        <header>total</header>
        <div className="value main">26m 20sec</div>
        <div className="value secondary">100%</div>
      </div>

      <div className="timer now">
        <header>now</header>
        <div className="value main">26m 20sec</div>
        <div className="value secondary">100%</div>
      </div>

      <div className="actions">
        <button className="btn start">start</button>
        <button className="btn pause">pause</button>
        <button className="btn stop">stop</button>
      </div>

      <TimeSlotList slots={ this.props.timeset.slots }/>
    </div>;
  }
}

export { TimeSetWidget }
