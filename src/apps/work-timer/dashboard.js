import { TimeSetWidget } from './time-set-widget';

class Dashboard extends React.Component {
  render() {
    return <div className="dashboard">
      {
        this.props.sets.map((ts) =>
          <TimeSetWidget key={ ts.id } timeset={ ts } />
        )
      }
    </div>;
  }
}

export { Dashboard }
