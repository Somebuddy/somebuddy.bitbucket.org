import { TaskWidget } from './task-widget';

class Todos extends React.Component {
  render() {
    return <div className="todos">
      <header>
        <h1>To-do list</h1>
      </header>
      <section className="tasks">
        <TaskWidget />
        <TaskWidget />
        <TaskWidget />
        <TaskWidget />
      </section>
    </div>;
  }
}

export { Todos }
