class TimeSlotWidget extends React.Component {
  render() {
    return <div className="time-slot row">
      <div className="useful-flag checked">
        <div className="icon"></div>
        <div className="label">was useful?</div>
      </div>

      <div className="period">
        <div className="dates">
          <div className="start">Dec 31, 2016</div>
          <div className="finish">Dec 31, 2016</div>
        </div>
        <div className="times">
          <div className="start">10:01</div>
          <div className="finish">12:00</div>
        </div>
      </div>

      <div className="comment variable">
        <div className="label">comment:</div>
        <div className="value">Sample comment</div>
      </div>

      <div className="timer variable">
        <div className="value main">26m 20sec</div>
        <div className="value secondary">100%</div>
      </div>

      <div className="actions">
        <button className="btn delete">Delete</button>
      </div>
    </div>;
  }
}

class TimeSlotList extends React.Component {
  render() {
    return <div className="time-slots">
      {
        this.props.slots.map((s) =>
          <TimeSlotWidget key={ s.id } slot={ s } />
        )
      }
    </div>;
  }
}

export { TimeSlotList, TimeSlotWidget }
