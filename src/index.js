import { WorkTimerPage } from './pages/index';

const element = <WorkTimerPage />;

ReactDOM.render(
  element,
  document.getElementById('timer')
);
