'use strict';

System.register(['./work-timer'], function (_export, _context) {
  "use strict";

  var WorkTimerPage;
  return {
    setters: [function (_workTimer) {
      WorkTimerPage = _workTimer.WorkTimerPage;
    }],
    execute: function () {
      _export('WorkTimerPage', WorkTimerPage);
    }
  };
});