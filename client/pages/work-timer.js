'use strict';

System.register(['../apps/work-timer/dashboard', '../apps/work-timer/todos'], function (_export, _context) {
  "use strict";

  var Dashboard, Todos, _createClass, TIMESETS, WorkTimerPage;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_appsWorkTimerDashboard) {
      Dashboard = _appsWorkTimerDashboard.Dashboard;
    }, function (_appsWorkTimerTodos) {
      Todos = _appsWorkTimerTodos.Todos;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      TIMESETS = [{
        id: 1,
        comment: 'Added time set stub template',
        running: true,
        slots: [{ id: 1 }, { id: 2 }]
      }, {
        id: 2,
        comment: 'Configure service worker',
        running: false,
        slots: [{ id: 3 }, { id: 4 }]
      }, {
        id: 3,
        comment: 'Add time set widget base styles',
        running: false,
        slots: [{ id: 5 }, { id: 6 }]
      }, {
        id: 4,
        comment: 'Add time set example content',
        running: false,
        slots: [{ id: 7 }, { id: 8 }]
      }];

      _export('WorkTimerPage', WorkTimerPage = function (_React$Component) {
        _inherits(WorkTimerPage, _React$Component);

        function WorkTimerPage() {
          _classCallCheck(this, WorkTimerPage);

          return _possibleConstructorReturn(this, (WorkTimerPage.__proto__ || Object.getPrototypeOf(WorkTimerPage)).apply(this, arguments));
        }

        _createClass(WorkTimerPage, [{
          key: 'render',
          value: function render() {
            return React.createElement(
              'section',
              { className: 'work-timer-app' },
              React.createElement(
                'nav',
                null,
                React.createElement(
                  'ul',
                  null,
                  React.createElement(
                    'li',
                    null,
                    React.createElement(
                      'a',
                      { href: '#' },
                      'Dashboard'
                    )
                  ),
                  React.createElement(
                    'li',
                    null,
                    React.createElement(
                      'a',
                      { href: '#' },
                      'History'
                    )
                  ),
                  React.createElement(
                    'li',
                    null,
                    React.createElement(
                      'a',
                      { href: '#' },
                      'Statistics'
                    )
                  ),
                  React.createElement(
                    'li',
                    null,
                    React.createElement(
                      'a',
                      { href: '#' },
                      'Settings'
                    )
                  )
                )
              ),
              React.createElement(
                'section',
                { className: 'content' },
                React.createElement(Dashboard, { sets: TIMESETS }),
                React.createElement(Todos, null)
              ),
              React.createElement('footer', null)
            );
          }
        }]);

        return WorkTimerPage;
      }(React.Component));

      _export('WorkTimerPage', WorkTimerPage);
    }
  };
});