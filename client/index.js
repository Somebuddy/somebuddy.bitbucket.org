'use strict';

System.register(['./pages/index'], function (_export, _context) {
  "use strict";

  var WorkTimerPage, element;
  return {
    setters: [function (_pagesIndex) {
      WorkTimerPage = _pagesIndex.WorkTimerPage;
    }],
    execute: function () {
      element = React.createElement(WorkTimerPage, null);


      ReactDOM.render(element, document.getElementById('timer'));
    }
  };
});