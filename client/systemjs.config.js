(function (global) {
  System.defaultJSExtensions = true;

  System.config({
    paths: {
      'npm:': 'node_modules/'
    },
    map: {
      app: 'client'
    },
    packages: {
      app: {
        main: './index.js',
        format: 'register',
        defaultExtension: 'js',
      },
    },
    meta: {
      '*.js': {
        react: true
      }
    }
  });
})(this);
