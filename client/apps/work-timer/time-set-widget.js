"use strict";

System.register(["./time-slot-widget"], function (_export, _context) {
  "use strict";

  var TimeSlotList, _createClass, TimeSetWidget;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_timeSlotWidget) {
      TimeSlotList = _timeSlotWidget.TimeSlotList;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export("TimeSetWidget", TimeSetWidget = function (_React$Component) {
        _inherits(TimeSetWidget, _React$Component);

        function TimeSetWidget() {
          _classCallCheck(this, TimeSetWidget);

          return _possibleConstructorReturn(this, (TimeSetWidget.__proto__ || Object.getPrototypeOf(TimeSetWidget)).apply(this, arguments));
        }

        _createClass(TimeSetWidget, [{
          key: "render",
          value: function render() {
            return React.createElement(
              "div",
              { className: "widget time-set" },
              React.createElement(
                "div",
                { className: "useful-flag checked" },
                React.createElement("div", { className: "icon" }),
                React.createElement(
                  "div",
                  { className: "label" },
                  "is useful?"
                )
              ),
              React.createElement(
                "div",
                { className: "comment variable" },
                React.createElement(
                  "div",
                  { className: "label" },
                  "comment:"
                ),
                React.createElement(
                  "div",
                  { className: "value" },
                  this.props.timeset.comment
                )
              ),
              React.createElement(
                "div",
                { className: "start-time" },
                React.createElement(
                  "header",
                  null,
                  "started at:"
                ),
                React.createElement(
                  "div",
                  { className: "value" },
                  "10:01"
                )
              ),
              React.createElement(
                "div",
                { className: "timer summary" },
                React.createElement(
                  "header",
                  null,
                  "total"
                ),
                React.createElement(
                  "div",
                  { className: "value main" },
                  "26m 20sec"
                ),
                React.createElement(
                  "div",
                  { className: "value secondary" },
                  "100%"
                )
              ),
              React.createElement(
                "div",
                { className: "timer now" },
                React.createElement(
                  "header",
                  null,
                  "now"
                ),
                React.createElement(
                  "div",
                  { className: "value main" },
                  "26m 20sec"
                ),
                React.createElement(
                  "div",
                  { className: "value secondary" },
                  "100%"
                )
              ),
              React.createElement(
                "div",
                { className: "actions" },
                React.createElement(
                  "button",
                  { className: "btn start" },
                  "start"
                ),
                React.createElement(
                  "button",
                  { className: "btn pause" },
                  "pause"
                ),
                React.createElement(
                  "button",
                  { className: "btn stop" },
                  "stop"
                )
              ),
              React.createElement(TimeSlotList, { slots: this.props.timeset.slots })
            );
          }
        }]);

        return TimeSetWidget;
      }(React.Component));

      _export("TimeSetWidget", TimeSetWidget);
    }
  };
});