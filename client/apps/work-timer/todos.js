"use strict";

System.register(["./task-widget"], function (_export, _context) {
  "use strict";

  var TaskWidget, _createClass, Todos;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_taskWidget) {
      TaskWidget = _taskWidget.TaskWidget;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export("Todos", Todos = function (_React$Component) {
        _inherits(Todos, _React$Component);

        function Todos() {
          _classCallCheck(this, Todos);

          return _possibleConstructorReturn(this, (Todos.__proto__ || Object.getPrototypeOf(Todos)).apply(this, arguments));
        }

        _createClass(Todos, [{
          key: "render",
          value: function render() {
            return React.createElement(
              "div",
              { className: "todos" },
              React.createElement(
                "header",
                null,
                React.createElement(
                  "h1",
                  null,
                  "To-do list"
                )
              ),
              React.createElement(
                "section",
                { className: "tasks" },
                React.createElement(TaskWidget, null),
                React.createElement(TaskWidget, null),
                React.createElement(TaskWidget, null),
                React.createElement(TaskWidget, null)
              )
            );
          }
        }]);

        return Todos;
      }(React.Component));

      _export("Todos", Todos);
    }
  };
});