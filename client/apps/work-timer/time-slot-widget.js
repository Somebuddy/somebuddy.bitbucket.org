"use strict";

System.register([], function (_export, _context) {
  "use strict";

  var _createClass, TimeSlotWidget, TimeSlotList;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export("TimeSlotWidget", TimeSlotWidget = function (_React$Component) {
        _inherits(TimeSlotWidget, _React$Component);

        function TimeSlotWidget() {
          _classCallCheck(this, TimeSlotWidget);

          return _possibleConstructorReturn(this, (TimeSlotWidget.__proto__ || Object.getPrototypeOf(TimeSlotWidget)).apply(this, arguments));
        }

        _createClass(TimeSlotWidget, [{
          key: "render",
          value: function render() {
            return React.createElement(
              "div",
              { className: "time-slot row" },
              React.createElement(
                "div",
                { className: "useful-flag checked" },
                React.createElement("div", { className: "icon" }),
                React.createElement(
                  "div",
                  { className: "label" },
                  "was useful?"
                )
              ),
              React.createElement(
                "div",
                { className: "period" },
                React.createElement(
                  "div",
                  { className: "dates" },
                  React.createElement(
                    "div",
                    { className: "start" },
                    "Dec 31, 2016"
                  ),
                  React.createElement(
                    "div",
                    { className: "finish" },
                    "Dec 31, 2016"
                  )
                ),
                React.createElement(
                  "div",
                  { className: "times" },
                  React.createElement(
                    "div",
                    { className: "start" },
                    "10:01"
                  ),
                  React.createElement(
                    "div",
                    { className: "finish" },
                    "12:00"
                  )
                )
              ),
              React.createElement(
                "div",
                { className: "comment variable" },
                React.createElement(
                  "div",
                  { className: "label" },
                  "comment:"
                ),
                React.createElement(
                  "div",
                  { className: "value" },
                  "Sample comment"
                )
              ),
              React.createElement(
                "div",
                { className: "timer variable" },
                React.createElement(
                  "div",
                  { className: "value main" },
                  "26m 20sec"
                ),
                React.createElement(
                  "div",
                  { className: "value secondary" },
                  "100%"
                )
              ),
              React.createElement(
                "div",
                { className: "actions" },
                React.createElement(
                  "button",
                  { className: "btn delete" },
                  "Delete"
                )
              )
            );
          }
        }]);

        return TimeSlotWidget;
      }(React.Component));

      _export("TimeSlotList", TimeSlotList = function (_React$Component2) {
        _inherits(TimeSlotList, _React$Component2);

        function TimeSlotList() {
          _classCallCheck(this, TimeSlotList);

          return _possibleConstructorReturn(this, (TimeSlotList.__proto__ || Object.getPrototypeOf(TimeSlotList)).apply(this, arguments));
        }

        _createClass(TimeSlotList, [{
          key: "render",
          value: function render() {
            return React.createElement(
              "div",
              { className: "time-slots" },
              this.props.slots.map(function (s) {
                return React.createElement(TimeSlotWidget, { key: s.id, slot: s });
              })
            );
          }
        }]);

        return TimeSlotList;
      }(React.Component));

      _export("TimeSlotList", TimeSlotList);

      _export("TimeSlotWidget", TimeSlotWidget);
    }
  };
});