"use strict";

System.register(["./time-set-widget"], function (_export, _context) {
  "use strict";

  var TimeSetWidget, _createClass, Dashboard;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_timeSetWidget) {
      TimeSetWidget = _timeSetWidget.TimeSetWidget;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export("Dashboard", Dashboard = function (_React$Component) {
        _inherits(Dashboard, _React$Component);

        function Dashboard() {
          _classCallCheck(this, Dashboard);

          return _possibleConstructorReturn(this, (Dashboard.__proto__ || Object.getPrototypeOf(Dashboard)).apply(this, arguments));
        }

        _createClass(Dashboard, [{
          key: "render",
          value: function render() {
            return React.createElement(
              "div",
              { className: "dashboard" },
              this.props.sets.map(function (ts) {
                return React.createElement(TimeSetWidget, { key: ts.id, timeset: ts });
              })
            );
          }
        }]);

        return Dashboard;
      }(React.Component));

      _export("Dashboard", Dashboard);
    }
  };
});